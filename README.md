README v0.1 / 21 JANUARY 2016

# Basic Text Operations

## Introduction

A super-basic java application to read .csv, .log and .xml files, process the data and output it again in a .csv file.

## Usage

Place some files in the input folder and add some java code to modify it inside the stream closure of the main class.

## Contributing

--

## Help

--

## Installation

### Requirements

- Java 8
- any Java IDE or your command line

### Installation

- download the code
- create a new project in your IDE and add the downloaded folder as existing sources 
(alternatively use your command line to compile and run the java code)

### Configuration

- modify the stream closure in the main class to your liking

## Credits

Lukas Klement

## Contact

@LukasKlement