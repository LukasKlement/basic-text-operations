package text.operations.run;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.LongAdder;
import text.operations.run.service.ReadMultiColumnFile;

/**
 *
 * @author Lukas Klement <igel.tech (Frescana UG)>
 */
public class CombineColumns {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        String filename = "input.csv";
        char delimiter = ';';
        int numberOfColumns = 2;
        String character = " ";
        
        List<String> adWordsKeywords = new ArrayList<>();
        LongAdder counter = new LongAdder();
        
        switch (numberOfColumns) {
            case 4:
                {
                    List<List<String>> list = ReadMultiColumnFile.readCsv(filename, delimiter, numberOfColumns);
                    list.get(0).stream().forEach((String material) -> {
                        list.get(1).stream().forEach((String adjective) -> {
                            list.get(2).stream().forEach((String action) -> {
                                list.get(3).stream().forEach((String ort) -> {
                                    if (material != null && !material.isEmpty() && adjective != null && action != null && !adjective.isEmpty() && !action.isEmpty() && ort != null && !ort.isEmpty()) {
                                        adWordsKeywords.add("\"" + material + character + adjective + character + action + character + ort + "\"");
                                        counter.increment();
                                    }
                                });
                            });
                        });
                    });     
                    break;
                }
            case 3:
                {
                    List<List<String>> list = ReadMultiColumnFile.readCsv(filename, delimiter, numberOfColumns);
                    list.get(0).stream().forEach((String material) -> {
                        list.get(1).stream().forEach((String adjective) -> {
                            list.get(2).stream().forEach((String action) -> {
                                if (material != null && !material.isEmpty() && adjective != null && action != null && !adjective.isEmpty() && !action.isEmpty()) {
                                    adWordsKeywords.add("\"" + material + character + adjective + character + action + "\"");
                                    counter.increment();
                                }
                            });
                        });
                    });     
                    break;
                }
            case 2:
                {
                    List<List<String>> list = ReadMultiColumnFile.readCsv(filename, delimiter, numberOfColumns);
                    list.get(0).stream().forEach((String material) -> {
                        list.get(1).stream().forEach((String adjective) -> {
                            if (material != null && !material.isEmpty() && adjective != null && !adjective.isEmpty()) {
                                adWordsKeywords.add("\"" + material + character + adjective + "\"");
                                counter.increment();
                            }
                        });
                    });     
                    break;
                }
            default:
                break;
        }
        
        adWordsKeywords.stream().forEach((String keyword) -> {
            System.out.println(keyword);
        });
        
        System.out.println("Number of generated keywords: " + counter.toString());
    }
    
}
