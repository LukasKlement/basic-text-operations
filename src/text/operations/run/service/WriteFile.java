package text.operations.run.service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author lukasklement
 */
public class WriteFile {
    
    /**
     * File Writer (.csv)
     * @param text the text to output in a csv file
     */
    public static void writeFile(List<String> text) {
        
        StringBuilder builder = new StringBuilder();
        text.stream().forEach((String line) -> {
            builder.append(line).append('\n');
        });
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("output/editedText.csv"))) {
            writer.write(builder.toString());
            writer.flush();
            writer.close();
        } catch(IOException e) {
             System.out.println("Error writing file:" + e.getMessage());
        }
        
    }
}