package text.operations.run.service;

import com.opencsv.CSVReader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

/**
 *
 * @author lukasklement
 */
public class ReadFile {
    
    /**
     * File Reader (.csv, .log)
     * @return a java list of text strings
     */
    public static List<String> readFiles() {
        return readFiles(null);
    }
    
    /**
     * File Reader (.xml)
     * @param argument the xml tag to parse
     * @return a java list of text strings
     */
    public static List<String> readFiles(String argument) {
        List<String> files = new ArrayList<>();
        List<String> text = new ArrayList<>();

        // Identify the files in the input folder
        File[] fileList = new File("input").listFiles();
        for (File file : fileList) {
            if (file.isFile() && (file.getName().endsWith(".csv") || file.getName().endsWith(".log") || (file.getName().endsWith(".xml") && argument != null))) {
                files.add(file.getName());
            } else if (file.getName().endsWith(".xml") && argument == null) {
                System.out.println("The following XML file can only be parsed if the tag to be parsed is passed as an argument (e.g. 'loc'): " + file.getName());
            } else if (file.isFile() && file.getName().startsWith(".")) {
                // ignore hidden files
            } else {
                System.out.println("The format of the following file is not supported: " + file.getName());
            }
        }

        // Parse links from the provided files
        files.stream().forEach((String file) -> {
            if (file.endsWith(".csv")) {
                text.addAll(readCsv(file));
            } else if (file.endsWith(".xml")) {
                text.addAll(readXml(file, argument));
            } else if (file.endsWith(".log")) {
                text.addAll(readLog(file));
            }
        });
        
        return text; 
    }
    
    /**
     * 
     * @param input the file to parse
     * @return list of text
     */
    public static List<String> readCsv(String input) {
        List<String> list = new ArrayList<>();
        try (CSVReader reader = new CSVReader(new FileReader("input/" + input))) {
            List<String[]> el = reader.readAll();            
            
            el.parallelStream().forEach((url) -> {
                list.add(url[0]);
            });
        } catch (IOException e) {
            System.out.println("Error reading URLs: " + e.getMessage());
        }
        return list;
    }

    /**
     *
     * @param input the file to parse
     * @return list of text
     */
    public static List<String> readLog(String input) {
        List<String> list = new ArrayList<>();
        try (FileInputStream fstream = new FileInputStream("input/" + input)) {
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            String line;
            while ((line = br.readLine()) != null)   {
                list.add(line);
            }
        } catch (IOException e) {
            System.out.println("Error reading URLs from log file: " + e.getMessage());
        }
        return list;
    }
    
    /**
     *
     * @param file the file to parse
     * @param argument the XML tag to parse
     * @return list of text
     */
    public static List<String> readXml(String file, String argument) {
        List<String> urls = new ArrayList<>();
        try {
            String inputFile = readXmlFile("input/" + file, StandardCharsets.UTF_8);
            Document doc = Jsoup.parse("input/" + inputFile, "", Parser.xmlParser());
            Elements el = doc.select(argument);
            el.stream().forEach(e -> {
                urls.add(e.text());
            });
        } catch (Exception e) {
            System.out.println("IO Error for XML file:" + e.getMessage());
        }
        return urls;
    }
    
    static String readXmlFile(String path, Charset encoding) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }
}