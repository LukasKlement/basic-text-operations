package text.operations.run.service;

import com.opencsv.CSVReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lukas Klement <igel.tech (Frescana UG)>
 */
public class ReadMultiColumnFile {
    /**
     * 
     * @param filename the file to parse
     * @param delimiter the CSV delimiter
     * @param numberOfColumns to parse
     * @return list of text
     */
    public static List<List<String>> readCsv(String filename, char delimiter, int numberOfColumns) {
        
        List<List<String>> list = new ArrayList<>();
        
        if (numberOfColumns == 0) {
            return list;
        } else {
            for (int i = 0; i < numberOfColumns; i++) {
                List<String> columnList = new ArrayList<>();
                try (CSVReader reader = new CSVReader(new FileReader("input/" + filename), delimiter)) {
                    List<String[]> el = reader.readAll();
                    final int current = i;
                    el.parallelStream().forEach((url) -> {
                        try {
                            columnList.add(url[current]);
                        } catch (Exception e) {
                            // ignore
                        }
                    });
                    list.add(columnList);
                } catch (IOException e) {
                    System.out.println("Error reading column " + (i + 1) + ": " + e.getMessage());
                }
            }
        }
        
        return list;
    }
}
