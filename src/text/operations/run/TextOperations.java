package text.operations.run;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.text.WordUtils;
import text.operations.run.service.ReadFile;
import text.operations.run.service.WriteFile;

/**
 *
 * @author lukasklement
 */
public class TextOperations {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List<String> editedText = new ArrayList<>();
        List<String> text = ReadFile.readFiles();
        text.stream().forEach((String line) -> {
            String editedLine = line.substring(line.indexOf("_") + 1, line.lastIndexOf("."));
            editedText.add(WordUtils.capitalize(editedLine));
        });
        
        // write only non-duplicates
        WriteFile.writeFile(editedText.stream().distinct().sorted().collect(Collectors.toList()));
    }
    
}